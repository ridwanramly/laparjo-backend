<?php

namespace App\Transformers;

use App\MenuOption;
use League\Fractal\TransformerAbstract;

class MenuOptionTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(MenuOption $option)
    {
        return [
            'menu_id' => $option->menu_id,
            'name'    => $option->name,
            'price'   => $option->price,
        ];
    }
}
