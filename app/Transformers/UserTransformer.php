<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    protected $availableIncludes = [
        'role',
    ];
    public function transform(User $user)
    {
        return [
            'id'      => $user->id,
            'name'    => $user->name,
            'email'   => $user->email,
            'role_id' => $user->role->description,
            'phone'   => $user->phone,
            'join'    => $user->created_at->diffForHumans(),
        ];
    }
    public function includeRole(User $user)
    {
        $role = $user->role;
        return $this->item($role, new RoleTransformer);
    }
}
