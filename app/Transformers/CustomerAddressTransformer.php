<?php

namespace App\Transformers;

use App\CustomerAddress;
use League\Fractal\TransformerAbstract;

class CustomerAddressTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */

    protected $availableIncludes = [
        'order',
    ];
    public function transform(CustomerAddress $address)
    {
        return [
            'user_id'        => $address->user_id;
            'street_address' => $address->street_address,
            'building_name'  => $address->building_name,
            'building_type'  => $address->building_type,
            'street_name'    => $address->street_name,
            'floor_unit'     => $address->floor_unit,
            'block'          => $address->block,
            'house_no'       => $address->house_no,
            'instruction'    => $address->instruction,
            'll'             => $address->ll,
        ];
    }
    public function order(CustomerAddress $address)
    {
        $order = $address->order;
        return $this->collection($order, new OrderTransformer);
    }
}
