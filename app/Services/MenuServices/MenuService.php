<?php

namespace App\Services\MenuServices;

use App\Menu;
use App\Transformers\MenuTransformer;
use Storage;

class MenuService
{
    public function index() {}
    public function store($request)
    {
        $image = null;
        if ($request->hasFile('file')) {
            # code...
            $image = self::uploadPhoto($request->image, $request->warung_id);
        }
        $menu  = new Menu;
        $store = $menu->create([
            'warung_id'       => $request->warung_id,
            'category_id'     => $request->category_id,
            'sub_category_id' => $request->sub_category_id,
            'name'            => $request->name,
            'image'           => $image,
            'price'           => $request->price,
            'description'     => $request->description,
            'calories'        => $request->calories,
            'likes'           => 0,
            'ratings'         => 0,
        ]);

        $fractal = fractal()
            ->item($store)
            ->transformWith(new MenuTransformer)
            ->toArray();

        return response()->json([
            'status' => 'success',
            'data'   => $fractal,
        ]);
    }
    public function update($request, $id) {}
    public function view($id) {}
    public function destroy($id) {}
    public function uploadPhoto($photo, $warung_id)
    {
        $file = md5(rand(8829229, 192298282).''.\Carbon\Carbon::now('Asia/Tokyo')).'.'.$photo->getOriginalClientExtension();
        $photo->storeAs('warung/'.$warung_id.'/menu/', $file, 'spaces');
        Storage::disk('spaces')->setVisibility('/warung/'.$warung_id.'/menu/'.$file, 'public');
        return '/warung/'.$warung_id.'/menu/'.$file;
    }
}
