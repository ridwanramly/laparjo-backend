<?php

namespace App\Http\Controllers\Runner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DeliverController extends Controller
{
    public function answer()
    {
        return response()->json([
            [
                'action'    => 'talk',
                'voiceName' => 'Damayanti',
                'text'      => 'Selamat petang ridwan, sila tinggalkan mesej selepas bip',
            ],
        ]);
    }
}
