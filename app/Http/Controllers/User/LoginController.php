<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Services\UserServices\PhoneLoginServices;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PhoneLoginServices $s, Request $request)
    {
        return $s->verifyPhoneNumber($request->number);
    }
    public function verifyCode(PhoneLoginServices $s, Request $request)
    {
        return $s->verifyCode($request->code, $request->number);
    }
}
