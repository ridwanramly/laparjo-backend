<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table    = 'menus';
    protected $fillable = [
        'warung_id',
        'category_id',
        'sub_category_id',
        'name',
        'image',
        'price',
        'description',
        'calories',
        'ratings',
        'likes',
    ];

    public function warung()
    {
        return $this->belongsTo('App\Warung');
    }
    public function category()
    {
        return $this->belongsTo('App\Category');
    }
    public function menuOptions()
    {
        return $this->hasMany('App\MenuOption');
    }
    public function orderItem()
    {
        return $this->hasMany('App\OrderItem');
    }
    public function subCategory()
    {
        return $this->belongsTo('App\SubCategory');
    }
}
