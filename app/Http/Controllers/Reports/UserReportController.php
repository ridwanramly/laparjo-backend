<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use App\Services\ReportServices\UserReportService;
use Illuminate\Http\Request;

class UserReportController extends Controller
{
    protected $service;
    public function __construct(UserReportService $service)
    {
        $this->service = $service;
    }
    public function total()
    {
        return $this->service->totalUser();
    }
}
