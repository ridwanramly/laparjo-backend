<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table    = 'categories';
    protected $fillable = [
        'name',
        'image',
    ];

    public function menu()
    {
        return $this->hasMany('App\Menu');
    }

    public function subCategory()
    {
        return $this->hasMany('App\SubCategory');
    }

}
