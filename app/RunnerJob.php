<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RunnerJob extends Model
{
    protected $table    = 'runner_jobs';
    protected $fillable = [
        'runner_id',
        'order_id',
        'accept',
    ];

    public function runner()
    {
        return $this->belongsTo('App\Runner');
    }
    public function order()
    {
        return $this->belongsTo('App\Order');
    }
}
