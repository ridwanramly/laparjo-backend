<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id', 'phone',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->belongsTo('App\Role');
    }
    public function warung()
    {
        return $this->hasOne('App\Warung');
    }
    public function phoneVerification()
    {
        return $this->hasOne('App\PhoneVerification');
    }
    public function address()
    {
        return $this->hasMany('App\CustomerAddress');
    }
    public function order()
    {
        return $this->hasMany('App\Order');
    }
}
