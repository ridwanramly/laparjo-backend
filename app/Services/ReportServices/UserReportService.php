<?php

namespace App\Services\ReportServices;

use App\User;

class UserReportService
{
    public function totalUser()
    {
        $user     = User::count();
        $runner   = User::where('role_id', 2)->count();
        $merchant = User::where('role_id', 3)->count();

        return response()->json([
            'message' => 'sucsess',
            'total'   => [
                'users'    => $user,
                'runner'   => $runner,
                'merchant' => $merchant,
            ],
        ]);
    }
}
