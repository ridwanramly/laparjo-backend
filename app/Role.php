<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const Customer         = 1;
    const Runner           = 2;
    const Warung           = 3;
    const Customer_Service = 4;
    const Moderator        = 5;
    const Admin            = 6;
    const Super_Admin      = 7;
    const Developer        = 8;

    protected $table    = 'roles';
    protected $fillable = [
        'level',
        'description',
    ];

    public function user()
    {
        return $this->hasMany('App\User');
    }
}
