<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRunnerLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('runner_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('runner_id')->unsigned();
            $table->integer('order_id')->unsigned();
            $table->string('current_ll');
            $table->foreign('runner_id')->references('id')->on('runners')->onDelete('cascade');
            $table->foreign('order_id')->references('id')->on('orders');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('runner_locations');
    }
}
