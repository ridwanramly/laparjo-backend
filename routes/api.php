<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'login'], function () {
    Route::post('/', 'User\LoginController@index');
    Route::post('/code', 'User\LoginController@verifyCode');
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::apiResource('warung', 'Warungs\MerchantController');
    Route::apiResource('category', 'Warungs\CategoryController');
    Route::apiResource('categories/sub', 'Warungs\SubCategoryController');
    Route::apiResource('users', 'User\UserController');
    Route::apiResource('menus', 'Warungs\MenuController');
    Route::group(['prefix' => 'reports'], function () {
        Route::get('/users', 'Reports\UserReportController@total');
    });
});
