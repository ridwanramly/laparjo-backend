<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuOption extends Model
{
    protected $table    = 'menu_options';
    protected $fillable = [
        'menu_id',
        'name',
        'price',
    ];
    public function menu()
    {
        return $this->belongsTo('App\Menu');
    }
}
