<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWarungOperatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warung_operatings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warung_id')->unsigned();
            $table->string('days');
            $table->time('open');
            $table->time('close');
            $table->timestamps();
            $table->foreign('warung_id')->references('id')->on('warungs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warung_operatings');
    }
}
