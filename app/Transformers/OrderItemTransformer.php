<?php

namespace App\Transformers;

use App\OrderItem;
use League\Fractal\TransformerAbstract;

class OrderItemTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */

    protected $availableIncludes = [
        'menu',
    ];
    public function transform(OrderItem $item)
    {
        return [
            'order_id' => $item->order_id,
            'menu_id'  => $item->menu_id,
            'quantity' => $item->quantity,
        ];
    }
}
