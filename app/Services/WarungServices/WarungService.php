<?php

namespace App\Services\WarungServices;

use App\Transformers\WarungTransformer;
use App\User;
use App\Warung;
use Auth;
use Storage;

class WarungService
{
    public function add($request)
    {
        $users = Auth::user()->role->description;
        // dd($user);
        if ($users == 'Admin' || $users == 'Super Admin' || $users == 'Customer Service' || $users == 'Moderator') {
            $user = User::find($request->user_id);
            if ($user->warung == null && $user->role_id == 3) {
                $image = null;
                if ($request->hasFile('image')) {
                    $image = self::uploadImage($request->image, $request->user_id);
                }
                $warung = new Warung;
                $create = $warung->create([
                    'user_id'  => $request->user_id,
                    'name'     => $request->name,
                    'address'  => $request->address,
                    'city'     => $request->city,
                    'state'    => $request->state,
                    'postcode' => $request->postcode,
                    'phone'    => $request->phone,
                    'image'    => $image,
                    'll'       => $request->ll,
                    'halal'    => $request->halal,
                ]);

                $fractal = fractal()
                    ->item($create)
                    ->transformWith(new WarungTransformer)
                    ->toArray();

                return response()->json([
                    'status' => 'success',
                    'data'   => $fractal,
                ]);
            } else {
                return response()->json([
                    'status'  => 'error',
                    'message' => 'Shop already exists or please assign user to shop role.',
                ]);
            }
        }

        return response()->json([
            'message' => 'U dont have access',
        ]);

    }
    public function view($id)
    {
        $warung = Warung::find($id);
        return fractal()
            ->item($warung)
            ->parseIncludes(['user', 'menu'])
            ->transformWith(new WarungTransformer)
            ->toArray();
    }
    public function edit($request, $id)
    {
        $users = Auth::user()->role->description;
        if ($users == 'Admin' || $users == 'Moderator' || $users == 'Super Admin' || $users == 'Warung') {
            $warung = Warung::find($id);
            $update = $warung->update([
                'name'     => $request->name,
                'address'  => $request->address,
                'city'     => $request->city,
                'state'    => $request->state,
                'postcode' => $request->postcode,
                'phone'    => $request->phone,
                'll'       => $request->ll,
            ]);

            return fractal()
                ->item($update)
                ->transformWith(new WarungTransformer)
                ->toArray();
        }

        return response()->json([
            'message' => 'U dont have access',
        ]);

    }
    public function delete($id)
    {
        $warung = Warung::find($id);
        if ($warung->delete()) {
            return response()->json([
                'message' => 'deleted',
            ]);
        } else {
            return response()->json([
                'message' => 'already deleted',
            ]);
        }
    }
    public function lists()
    {
        $warung = Warung::all();
        return fractal()
            ->collection($warung)
            ->parseIncludes(['user', 'menu'])
            ->transformWith(new WarungTransformer)
            ->toArray();
    }
    public function uploadImage($image, $warung)
    {
        $name  = md5(rand(0, 99999)).'.'.$image->getClientOriginalExtension();
        $store = $image->storeAs('image/warung/'.$warung.'/images', $name, 'spaces');
        Storage::disk('spaces')->setVisibility('image/warung/'.$warung.'/images/'.$name, 'public');
        return 'image/warung/'.$warung.'/images/'.$name;
    }
}
