<?php

namespace App\Services\WarungServices;

use App\SubCategory;
use App\Transformers\SubCategoryTransformer;
use Carbon\Carbon;
use Storage;

class SubCategoryService
{
    public function index()
    {
        $category = SubCategory::all();
        return fractal()
            ->collection($category)
            ->transformWith(new SubCategoryTransformer)
            ->toArray();
    }
    public function store($request)
    {
        $image    = null;
        $category = new SubCategory;
        if ($request->hasFile('image')) {
            $image = self::uploadPhoto($request->image);
        }
        $store = $category->create([
            'category_id' => $request->category_id,
            'name'        => $request->name,
            'image'       => $image,
        ]);

        $fractal = fractal()
            ->item($store)
            ->transformWith(new SubCategoryTransformer)
            ->toArray();

        return response()->json([
            'message' => 'success',
            'data'    => $fractal,
        ]);
    }
    public function view($id)
    {
        $category = SubCategory::find($id);
        return fractal()
            ->item($category)
            ->parseIncludes(['menu'])
            ->transformWith(new SubCategoryTransformer)
            ->toArray();
    }
    public function update($request, $id) {}
    public function destroy($id)
    {
        $category = SubCategory::find($id);
        return $category->delete();
    }
    public function uploadPhoto($image)
    {
        $name  = md5(rand(0, 99732081).''.Carbon::now('Asia/Kuala_Lumpur')).'.'.$image->getClientOriginalExtension();
        $store = $image->storeAs('image/category/sub', $name, 'spaces');
        Storage::disk('spaces')->setVisibility('/image/category/sub/'.$name, 'public');
        return '/image/category/sub/'.$name;
    }
}
