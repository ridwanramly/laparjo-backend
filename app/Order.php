<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const COD            = 0;
    const ONLINE_BANKING = 1;
    const CARD           = 2;
    const WALLET         = 3;

    const RECEIVED   = 0;
    const IN_MAKING  = 1;
    const PICKUP     = 2;
    const DELIVIRING = 3;
    const DELIVERED  = 4;
    const CANCEL     = 5;

    protected $table    = 'orders';
    protected $fillable = [
        'user_id',
        'address_id',
        'slot_id',
        'payment_type',
        'coupon',
        'total',
        'discount',
        'delivery_charge',
        'status',

    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function address()
    {
        return $this->belongsTo('App\CustomerAddress');
    }
    public function slot()
    {
        return $this->belongsTo('App\DeliverySlot');
    }
    public function orderItem()
    {
        return $this->hasMany('App\OrderItem');
    }
    public function runnerJob()
    {
        return $this->hasMany('App\RunnerJob');
    }
    public function runnerLocation()
    {
        return $this->hasOne('App\RunnerLocation');
    }
}
