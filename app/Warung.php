<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warung extends Model
{
    protected $table    = 'warungs';
    protected $fillable = [
        'user_id',
        'name',
        'address',
        'city',
        'state',
        'postcode',
        'phone',
        'image',
        'll',
        'halal',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function menu()
    {
        return $this->hasMany('App\Menu');
    }
    public function warungOperating()
    {
        return $this->hasMany('App\WarungOperating');
    }
}
