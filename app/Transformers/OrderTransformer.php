<?php

namespace App\Transformers;

use App\Order;
use League\Fractal\TransformerAbstract;

class OrderTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    protected $availableIncludes = [
        'address',
        'slot',
        'orderItem',
        'runnerJob',
        'runnerLocation',
    ];
    public function transform(Order $order)
    {
        return [
            'user_id'         => $order->user_id,
            'address_id'      => $order->address_id,
            'slot_id'         => $order->slot_id,
            'payment_type'    => $order->payment_type,
            'coupon'          => $order->coupun,
            'total'           => $order->total,
            'discount'        => $order->discount,
            'delivery_charge' => $order->delivery_charge,
            'status'          => $order->status,
        ];
    }
    public function includeAddress(Order $order)
    {
        $address = $order->address;
        return $this->item($address, new CustomerAddressTransfomer);
    }
    public function includeSlot(Order $order)
    {
        $slot = $order->slot;
        return $this->item($slot, new DeliverySlotTransformer);
    }
    public function includeOrderItem(Order $order)
    {
        $item = $order->orderItem;
        return $this->collection($item, new OrderItemTransformer);
    }
}
