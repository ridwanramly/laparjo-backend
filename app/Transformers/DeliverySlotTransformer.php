<?php

namespace App\Transformers;

use App\DeliverySlot;
use League\Fractal\TransformerAbstract;

class DeliverySlotTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(DeliverySlot $slot)
    {
        return [
            'time' => $slot->time,
        ];
    }
}
