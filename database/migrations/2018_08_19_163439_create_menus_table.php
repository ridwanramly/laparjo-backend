<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warung_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->string('name');
            $table->string('image')->nullable();
            $table->decimal('price');
            $table->string('description')->nullable();
            $table->string('calories')->nullable();
            $table->decimal('ratings')->default(0)->nullable();
            $table->integer('likes')->default(0)->nullable();
            $table->timestamps();
            $table->foreign('warung_id')->references('id')->on('warungs')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
