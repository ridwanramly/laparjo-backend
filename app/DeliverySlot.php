<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliverySlot extends Model
{
    protected $table    = 'delivery_slots';
    protected $fillable = [
        'time',
    ];

    public function order()
    {
        return $this->hasMany('App\Order');
    }
}
