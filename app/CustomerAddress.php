<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerAddress extends Model
{
    //
    const HIGH_RISE = 0;
    const LANDED    = 1;

    protected $table    = 'customer_addresses';
    protected $fillable = [
        'user_id',
        'street_address',
        'building_name',
        'building_type',
        'street_name',
        'floor_unit',
        'block',
        'house_no',
        'instruction',
        'll',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function order()
    {
        return $this->hasMany('App\Order');
    }
}
