<?php

namespace App\Transformers;

use App\Category;
use League\Fractal\TransformerAbstract;
use Storage;

class CategoryTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    protected $availableIncludes = [
        'menu',
        'sub',
    ];
    public function transform(Category $category)
    {
        return [
            'id'    => $category->id,
            'name'  => $category->name,
            'image' => Storage::disk('spaces')->url($category->image),
        ];
    }
    public function includeMenu(Category $category)
    {
        $menu = $category->menu;
        return $this->collection($menu, new MenuTransformer);
    }
    public function includeSub(Category $category)
    {
        $sub = $category->subCategory;
        return $this->collection($sub, new SubCategoryTransformer);
    }
}
