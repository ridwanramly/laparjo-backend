<?php

namespace App\Services\WarungServices;

use App\Category;
use App\Transformers\CategoryTransformer;
use Carbon\Carbon;
use Storage;

class CategoryService
{
    public function index()
    {
        $category = Category::all();
        return fractal()
            ->collection($category)
            ->transformWith(new CategoryTransformer)
            ->toArray();
    }
    public function store($request)
    {
        $image    = null;
        $category = new Category;
        if ($request->hasFile('image')) {
            $image = self::uploadPhoto($request->image);
        }
        $store = $category->create([
            'name'  => $request->name,
            'image' => $image,
        ]);

        $fractal = fractal()
            ->item($store)
            ->transformWith(new CategoryTransformer)
            ->toArray();

        return response()->json([
            'message' => 'success',
            'data'    => $fractal,
        ]);
    }
    public function view($id)
    {
        $category = Category::find($id);
        return fractal()
            ->item($category)
            ->parseIncludes(['sub'])
            ->transformWith(new CategoryTransformer)
            ->toArray();
    }
    public function update($request, $id) {}
    public function destroy($id)
    {
        $category = Category::find($id);
        return $category->delete();
    }
    public function uploadPhoto($image)
    {
        $name  = md5(rand(0, 999999).''.Carbon::now('Asia/Kuala_Lumpur')).'.'.$image->getClientOriginalExtension();
        $store = $image->storeAs('image/category', $name, 'spaces');
        Storage::disk('spaces')->setVisibility('image/category/'.$name, 'public');
        return 'image/category/'.$name;
    }
}
