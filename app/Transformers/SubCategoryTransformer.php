<?php

namespace App\Transformers;

use App\SubCategory;
use League\Fractal\TransformerAbstract;

class SubCategoryTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */

    protected $availableIncludes = [
        'menu',
    ];
    public function transform(SubCategory $sub)
    {
        return [
            'id'          => $sub->id,
            'category_id' => $sub->category_id,
            'name'        => $sub->name,
            'image'       => $sub->image,
        ];
    }
    public function includeMenu(SubCategory $sub)
    {
        $menu = $sub->menu;
        return $this->collection($menu, new MenuTransformers);
    }
}
