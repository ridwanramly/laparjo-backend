<?php

namespace App\Transformers;

use App\Warung;
use Illuminate\Support\Facades\Storage;
use League\Fractal\TransformerAbstract;

class WarungTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    protected $availableIncludes = [
        'user', 'menu',
    ];
    public function transform(Warung $w)
    {
        return [
            'id'       => $w->id,
            'name'     => $w->name,
            'address'  => $w->address,
            'city'     => $w->city,
            'state'    => $w->state,
            'postcode' => $w->postcode,
            'phone'    => $w->phone,
            'image'    => Storage::disk('spaces')->url($w->image),
            'll'       => $w->ll,
            'halal'    => $w->halal,
        ];
    }

    public function includeUser(Warung $w)
    {
        return $this->item($w->user, new UserTransformer);
    }
    public function includeMenu(Warung $w)
    {
        return $this->collection($w->menu, new MenuTransformer);
    }
}
