<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RunnerLocation extends Model
{
    protected $table    = 'runner_locations';
    protected $fillable = [
        'runner_id',
        'order_id',
        'current_id',
    ];

    public function runner()
    {
        return $this->belongsTo('App\Runner');
    }
    public function order()
    {
        return $this->belongsTo('App\Order');
    }
}
