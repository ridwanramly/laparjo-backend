<?php
namespace App\Services\UserServices;

use App\Transformers\UserTransformer;
use App\User;

class UserService
{
    public function index()
    {
        $users = User::all();
        return fractal()
            ->collection($users)
            ->transformWith(new UserTransformer)
            ->toArray();
    }
    public function store($request)
    {
        $user  = new User;
        $store = $user->create([
            'name'     => $request->name,
            'email'    => $request->email,
            'phone'    => $request->phone,
            'password' => bcrypt($request->phone),
            'role_id'  => $request->role,
        ]);

        return response()->json([
            'message' => 'success',
        ]);
    }
    public function update($request, $id)
    {
        $user   = User::find($id);
        $update = $user->update([
            'name'    => $request->name,
            'email'   => $request->email,
            'phone'   => $request->phone,
            'role_id' => $request->role,
        ]);

        return fractal()
            ->item($update)
            ->transformWith(new UserTransformer)
            ->toArray();
    }
    public function view($id)
    {
        $user = User::find($id);
        return fractal()
            ->item($user)
            ->transformWith(new UserTransformer)
            ->toArray();
    }
    public function destroy($id)
    {
        $user = User::find($id);
        return $user->delete();
    }
}
