<?php

namespace App\Transformers;

use App\Menu;
use League\Fractal\TransformerAbstract;

class MenuTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    protected $availableIncludes = [
        'menuOption',
        'orderItem',
    ];
    public function transform(Menu $menu)
    {
        return [
            'warung_id'   => $menu->warung_id,
            'category_id' => $menu->category_id,
            'name'        => $menu->name,
            'image'       => $menu->image,
            'price'       => $menu->price,
            'description' => $menu->description,
            'calories'    => $menu->calories,
            'ratings'     => $menu->ratings,
            'likes'       => $menu->likes,
        ];
    }
    public function includeMenuOption(Menu $menu)
    {
        $option = $menu->menuOptions;
        return $this->collection($option, new MenuOptionTransfomer);
    }
    public function includeOrderItem(Menu $menu)
    {
        $item = $menu->orderItem;
        return $this->collection($item, new OrderItemTransformer);
    }
}
