<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Runner extends Model
{
    protected $table    = 'runners';
    protected $fillable = [
        'user_id',
        'nickname',
        'image',
        'plate_number',
        'avalaibe',
        'current_ll',
        'status',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function runnerJob()
    {
        return $this->hasMany('App\RunnerJob');
    }
    public function runnerLocation()
    {
        return $this->hasMany('App\RunnerLocation');
    }
}
