<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhoneVerification extends Model
{
    protected $table    = 'phone_verifications';
    protected $fillable = [
        'user_id',
        'code',
        'expire',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
