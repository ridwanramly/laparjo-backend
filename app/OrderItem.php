<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table    = 'order_items';
    protected $fillable = [
        'order_id',
        'menu_id',
        'quantity',
    ];
    public function order()
    {
        return $this->belongsTo('App\Order');
    }
    public function menu()
    {
        return $this->belongsTo('App\Menu');
    }
}
