<?php

namespace App\Services\UserServices;

use App\PhoneVerification;
use App\User;

class PhoneLoginServices
{
    protected $client;
    protected $verification;
    public function __construct()
    {
        $this->client = new \Nexmo\Client(new \Nexmo\Client\Credentials\Basic(env('NEXMO_KEY'), env('NEXMO_SECRET')));
    }
    public function verifyPhoneNumber($number)
    {
        $user = User::where('phone', $number)->first();
        if ($user == null) {
            # code...
            return response()->json([
                'message' => 'Account not found',
                'status'  => 0,
            ]);
        } else {
            self::sentVerificationCode($number, $user->id);
            return response()->json([
                'message' => 'success',
                'status'  => 1,
            ]);
        }
    }

    public function sentVerificationCode($number, $user_id)
    {

        $verification = $this->client->verify()->start([
            'number'     => $number,
            'brand'      => 'laparjo',
            'pin_expiry' => '300',
        ]);

        $verify = PhoneVerification::where('user_id', $user_id)->first();
        if ($verify == null) {
            $verify->create([
                'user_id' => $user_id,
                'code'    => $verification['request_id'],
            ]);
        } else {
            $verify->update([
                'code' => $verification['request_id'],
            ]);
        }

        return $verify;

    }
    public function verifyCode($code, $number)
    {
        $user = User::where('phone', $number)->first();

        $check = $this->client->verify()->check($user->phoneVerification->code, $code);

        if ($check['status'] == '0') {
            # code...
            return $user->createToken('PhoneToken')->accessToken;
        } else {
            return $check['error_text'];
        }
    }
}
