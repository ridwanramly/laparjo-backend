<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WarungOperating extends Model
{
    protected $table    = 'warung_operatings';
    protected $fillable = [
        'warung_id',
        'days',
        'open',
        'close',
    ];
    public function warung()
    {
        return $this->belongsTo('App\Warung');
    }
}
